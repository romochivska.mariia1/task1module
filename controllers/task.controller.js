const Trepezium = require('../models/calculations');

exports.calculateArea = async (req, res) => {
  try {
    const { a, b, alpha } = req.query;
    const area = calculateTrepeziumArea(parseFloat(a), parseFloat(b), parseFloat(alpha));
    
    const Trepezium = new Trepezium({ a, b, alpha, area });
    await Trepezium.save();
    
    res.json({ area });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

function calculateTrepeziumArea(a, b, alpha) {
    const radians = alpha * (Math.PI / 180); 
    const h = Math.abs(b - a) * Math.sin(radians); 
    return ((a + b) / 2) * h; 
}
