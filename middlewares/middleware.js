exports.checkInput = (req, res, next) => {
  const { a, b, alpha } = req.query;

  if (isNaN(parseFloat(a)) || isNaN(parseFloat(b)) || isNaN(parseFloat(alpha))) {
    return res.status(400).json({ error: 'Parameters a, b, and alpha must be numbers' });
  }

  next();
};
