const mongoose = require('mongoose');

const trapeziumSchema = new mongoose.Schema({
  a: Number,
  b: Number,
  alpha: Number,
  area: Number,
  timestamp: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Trapezium', trapeziumSchema);

