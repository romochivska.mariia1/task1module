const express = require('express');
const router = express.Router();
const taskController = require('../controllers/task.controller');
const { checkInput } = require('../middlewares/middleware');

router.post('/calculateArea', checkInput, taskController.calculateArea);

module.exports = router;
