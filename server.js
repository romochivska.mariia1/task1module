const express = require('express');
const mongoose = require('mongoose');
const app = express();
const { port, mongodb_uri } = require('./config');
const taskRouter = require('./routes/task.route');

mongoose.connect(mongodb_uri)
  .then(() => {
    console.log('Mongo DB connected');
  });



app.use(express.json());

app.use('/calculateArea',taskRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});