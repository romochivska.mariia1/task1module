const Calculation = require('../models/сalculation');
const createError = require('http-errors');

exports.calculateSquare = async (req, res, next) => {
  try {
    const { a, b, alpha } = req.query;
    const area = calculateTrepeziumArea(parseFloat(a), parseFloat(b), parseFloat(alpha));
    
    const calculation = new Calculation({ a, b, alpha, area });
    await calculation.save();
    
    res.json({ area });
  } catch (err) {
    console.error(err);
    next(createError.InternalServerError('Internal server error'));
  }
};

function calculateTrepeziumArea(a, b, alpha) {
    const radians = alpha * (Math.PI / 180); // Переведення кута в радіани
    const h = Math.abs(b - a) * Math.sin(radians); // Висота трапеції
    return ((a + b) / 2) * h; // Площа трапеції
}

exports.checkInput = (req, res, next) => {
    const { a, b, alpha } = req.query;
  
    // Перевірка чи a, b та alpha є числами
    if (isNaN(parseFloat(a)) || isNaN(parseFloat(b)) || isNaN(parseFloat(alpha))) {
      return next(createError.BadRequest('Parameters a, b, and alpha must be numbers'));
    }
  
    next();
};
